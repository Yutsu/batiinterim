<?php

namespace BiEtBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ArtisanType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('NomArtisan')
                ->add('PrenomArtisan')
                ->add('datenaissanceartisan')
                ->add('VilleNaissanceArtisan')
                ->add('PaysNaissanceArtisan')
                ->add('TelephoneArtisan')
                ->add('adresseartisan')
                ->add('CpArtisan')
                ->add('villeartisan')
                ->add('loginartisan')
                ->add('mdpartisan')
                ->add('premiereconnexion')
                ->add('idcorpsmetier');
                //->add('idmission');
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BiEtBundle\Entity\Artisan'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'bietbundle_artisan';
    }


}
