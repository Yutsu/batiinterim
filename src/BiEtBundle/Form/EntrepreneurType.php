<?php

namespace BiEtBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EntrepreneurType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('raisonsociale')
                ->add('nomresponsable')
                ->add('prenomresponsable')
                ->add('telresponsable')
                ->add('telephoneentreprise')
                ->add('adresseentreprise')
                ->add('cpentreprise')
                ->add('villeentreprise')
                //->add('loginentreprise')
                //->add('mdpentreprise')
                ->add('premiereconnexion')
                ->add('idsecteur');
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BiEtBundle\Entity\Entrepreneur'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'bietbundle_entrepreneur';
    }


}
