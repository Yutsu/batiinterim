<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace BiEtBundle\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use BiEtBundle\Form\ChantierType;
use BiEtBundle\Entity\Chantier;

/**
 * Description of BatiInterimEntrepreneurController
 *
 * @author developpeur
 */

class BatiInterimEntrepreneurController extends Controller{

    public function accueilAction(){
        return $this->render('@BiEt/Entrepreneur/connexion.html.twig');
    }
    
    public function accueilEntrepreneurAction(){
        return $this->render('@BiEt/Entrepreneur/accueilEntrepreneur.html.twig');
    }
    
    public function afficherArtisanMetierAction(){
        $em = $this->getDoctrine()
                   ->getManager();

        $afficherArtisan = $em->getRepository('BiEtBundle:Artisan')
                              ->getArtisanMetier();
        

        return $this->render('@BiEt/Entrepreneur/afficherArtisanMetier.html.twig', array
                ('afficherArtisan'=>$afficherArtisan));
    }
    
    public function ajouterChantierAction(Request $request){
        $chantier = new Chantier();
        
        $form = $this->createForm(ChantierType::class, $chantier)
                     ->add('Envoyer', SubmitType::class);

        $form->handleRequest($request);
        
        if($form->isSubmitted()){
            $em = $this->getDoctrine()
                        ->getManager();
            
            $em->persist($chantier);
            $em->flush();
            return new response('Le chantier a bien été ajouté');
        }
        
        $formView = $form->createView();
        return $this->render('@BiEt/Entrepreneur/ajouterChantier.html.twig', array
            ('form'=>$formView));    
        
        }
    

}
