<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace BiEtBundle\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use BiEtBundle\Entity\Artisan;
use BiEtBundle\Form\ArtisanType;
use BiEtBundle\Entity\Entrepreneur;
use BiEtBundle\Form\EntrepreneurType;




/**
 * Description of BatiInterimController
 *
 * @author developpeur
 */
class BatiInterimController extends Controller{
    //Ajouter un secteur dans la base de donnée
    public function addArtisanAction(Request $request){
        $addArtisan = new Artisan();
        
        $form = $this->createForm(ArtisanType::class, $addArtisan)
                     ->add('Envoyer', SubmitType::class);

        $form->handleRequest($request);
        
        if($form->isSubmitted()){
            $em = $this->getDoctrine()
                        ->getManager();
            
            $em->persist($addArtisan);
            $em->flush();
            return new Response('Artisan ajouté !');
        }
        
        $formView = $form->createView();
        return $this->render('@BiEt/Gestionnaire/ajouterArtisan.html.twig', array
            ('form'=>$formView));
    }
    
    public function addEntrepreneurAction(Request $request){
        $addEntrepreneur = new Entrepreneur();
        
        $form = $this->createForm(EntrepreneurType::class, $addEntrepreneur)
                     ->add('Envoyer', SubmitType::class);

        $form->handleRequest($request);
        
        if($form->isSubmitted()){
            $em = $this->getDoctrine()
                        ->getManager();
            
            $em->persist($addEntrepreneur);
            $em->flush();
            return new Response('Entrepreneur ajouté !');
        }
        
        $formView = $form->createView();
        return $this->render('@BiEt/Gestionnaire/ajouterEntrepreneur.html.twig', array
            ('form'=>$formView));
    }
    
        public function supprimerArtisanAction(Request $request){
        $form = $this->createFormBuilder()
                ->add('artisan', EntityType::class,                      
                        array(  'label'=>'Artisan',
                                'class'=>'BiEtBundle:Artisan',
                                'choice_label'=>'nomArtisan',
                                'multiple'=>false))             
                ->add('Supprimer', SubmitType::class)               
                ->getForm();  
        
        $form->handleRequest($request); 
 
        if ($form->isSubmitted()) {   
        // Récupérer l’id correspondant à l’item dans la liste déroulante             
            $id = $form->get('artisan')
                       ->getData()
                       ->getId();
            
            $em = $this->getDoctrine()
                       ->getEntityManager();              
            
        // Récupérer l’objet d’entité correspondant    
            $artisan = $em->getRepository('BiEtBundle:Artisan')
                        ->find($id);
            
        // Supprimer l’objet d’entité             
            $em->remove($artisan);
            $em->flush();
        
            return $this->redirectToRoute('bi_et_afficheArtisan');
    }   
        
        return $this->render('@BiEt/Gestionnaire/supprimerArtisan.html.twig', 
            array('form'=>$form->createView()));
            
        
        
    }
    
        public function supprimerEntrepreneurAction(Request $request){
        $form = $this->createFormBuilder()
                ->add('entrepreneur', EntityType::class,                      
                        array(  'label'=>'Entrepreneur',
                                'class'=>'BiEtBundle:Entrepreneur',
                                'choice_label'=>'nomResponsable',
                                'multiple'=>false))             
                ->add('Supprimer', SubmitType::class)               
                ->getForm();  
        
        $form->handleRequest($request); 
 
        if ($form->isSubmitted()) {   
        // Récupérer l’id correspondant à l’item dans la liste déroulante             
            $id = $form->get('entrepreneur')
                       ->getData()
                       ->getId();
            
            $em = $this->getDoctrine()
                       ->getEntityManager();              
            
        // Récupérer l’objet d’entité correspondant    
            $entrepreneur = $em->getRepository('BiEtBundle:Entrepreneur')
                        ->find($id);
            
        // Supprimer l’objet d’entité             
            $em->remove($entrepreneur);
            $em->flush();
        
            return $this->redirectToRoute(' bi_et_afficheEntrepreneur');
            
        }   
        
        return $this->render('@BiEt/Gestionnaire/supprimerEntrepreneur.html.twig', 
                array('form'=>$form->createView()));
    }
    
}
