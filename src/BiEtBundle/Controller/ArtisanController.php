<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace BiEtBundle\Controller;


use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use BiEtBundle\Entity\Conge;
use BiEtBundle\Form\CongeType;
use BiEtBundle\Form\ArtisanType;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Description of ArtisanController
 *
 * @author developpeur
 */
    
class ArtisanController extends Controller {
    public function connexionArtisanAction(Request $requete, UserPasswordEncoderInterface $encoder, Artisan $artisan){
        $form = $this->createFormBuilder()
                ->add('login', TextType::class)
                ->add('mdp', PasswordType::class)
                ->add('Envoyer', SubmitType::class)
                ->getForm();
                
        $form->handleRequest($requete);

        if($form->isSubmitted()){
            
            $data = $form->getData();
            $login = $data['login'];
            
            if(($login->getNomArtisan() | slice(0,1)) . $login->getPrenomArtisan()){
                        
                $artisan->setPremiereConnexion(1);
                    
                $em = $this->getDoctrine()->getManager();

                $hash = $encoder->encodePassword($artisan, $artisan->getMdpartisan());
                $artisan->setMdpartisan($hash);     
                    
                $em->persist($artisan);
                $em->flush();
            }   
            
        return $this->render('@BiEt/Artisan/accueilArtisan.html.twig');
      }        
    return $this->render('@BiEt/Artisan/connexion.html.twig', array('form'=>$form->createView()));
    }
    
    }
