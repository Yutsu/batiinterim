<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace BiEtBundle\Controller;


use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use BiEtBundle\Entity\Conge;
use BiEtBundle\Form\CongeType;
use BiEtBundle\Form\ArtisanType;


/**
 * Description of ArtisanController
 *
 * @author developpeur
 */
class BatiInterimArtisanController extends Controller{
    
    public function connexionArtisanAction(Request $requete){
                
        $form = $this->createFormBuilder()
                ->add('login', TextType::class)
                ->add('mdp', PasswordType::class)
                ->add('Envoyer', SubmitType::class)
                ->getForm();
                
        $form->handleRequest($requete);

 
        if($form->isSubmitted()){
            $data = $form->getData();
            $login = $data['login'];
            $mdp = $data['mdp'];

            //Requête
            $em = $this->getDoctrine()
                        ->getManager();

            $em->getRepository('BiEtBundle:Artisan')
               ->getConnexionArtisan($login,$mdp);

            return $this->render('@BiEt/Artisan/accueilArtisan.html.twig');            
      }        
        return $this->render('@BiEt/Artisan/connexion.html.twig', array('form'=>$form->createView()));  
    }
    
    public function accueilArtisanAction(){
        return $this->render('@BiEt/Artisan/accueilArtisan.html.twig');            
    }
    
    
    public function gererCongesAction(){
        $repository1 = $this->getDoctrine()
                            ->getRepository('BiEtBundle:Conge');         
        $lesConges = $repository1->findAll();

        return $this->render('@BiEt/Artisan/gererConges.html.twig', array
                ('lesConges'=>$lesConges));
    }
    
    
    public function afficherMissionAction(){
        $repository = $this->getDoctrine()
                            ->getRepository('BiEtBundle:Mission');         
        $lesMissions = $repository->findAll();

        return $this->render('@BiEt/Artisan/consulterMission.html.twig', array
                ('lesMissions'=>$lesMissions));
    }
    
    public function addCongeAction(Request $request){
        $conge = new Conge();
        
        $form = $this->createForm(CongeType::class, $conge)
                     ->add('Envoyer', SubmitType::class);

        $form->handleRequest($request);
        
        if($form->isSubmitted()){
            $em = $this->getDoctrine()
                        ->getManager();
            
            $em->persist($conge);
            $em->flush();
            return $this->redirectToRoute('bi_et_gererCongesArtisan');
        }
        
        $formView = $form->createView();
        return $this->render('@BiEt/Artisan/ajouterConge.html.twig', array
            ('form'=>$formView));
    }
    
    
    public function supprimerCongeAction(Request $request){
        $form = $this->createFormBuilder()
                ->add('conge', EntityType::class,                      
                        array(  'label'=>'Conge',
                                'class'=>'BiEtBundle:Conge',
                                'choice_label'=>'id',
                                'multiple'=>false))             
                ->add('Supprimer', SubmitType::class)               
                ->getForm();  
        
        $form->handleRequest($request); 
 
        if ($form->isSubmitted()) {   
        // Récupérer l’id correspondant à l’item dans la liste déroulante             
            $id = $form->get('conge')
                       ->getData()
                       ->getId();
            
            $em = $this->getDoctrine()
                       ->getEntityManager();              
            
        // Récupérer l’objet d’entité correspondant    
            $conge = $em->getRepository('BiEtBundle:Conge')
                        ->find($id);
            
        // Supprimer l’objet d’entité             
            $em->remove($conge);
            $em->flush();
        
            return $this->redirectToRoute('bi_et_gererCongesArtisan');
    }   
        
        return $this->render('@BiEt/Artisan/supprimerConge.html.twig', 
            array('form'=>$form->createView()));
    }
    
    
    public function modifierCoordonneeAction(Request $request){
        
    $em = $this->getDoctrine()
               ->getManager();
    
    $artisan = $em->getRepository('BiEtBundle:Artisan')
                  ->find(1);
    
    //return new Response(dump($artisan));
   
    $form = $this->createForm(ArtisanType::class, $artisan)
                 ->add('Envoyer', SubmitType::class);

    $form->handleRequest($request);
        
    if($form->isSubmitted()){
            $artisan->setNomartisan($form->get('NomArtisan')->getData())
                    ->setPrenomartisan($form->get('PrenomArtisan')->getData())
                    ->setDatenaissanceartisan($form->get('datenaissanceartisan')->getData())
                    ->setVillenaissanceartisan($form->get('VilleNaissanceArtisan')->getData())
                    ->setPaysnaissanceartisan($form->get('PaysNaissanceArtisan')->getData())
                    ->setTelephoneartisan($form->get('TelephoneArtisan')->getData())
                    ->setAdresseartisan($form->get('adresseartisan')->getData())
                    ->setCpartisan($form->get('CpArtisan')->getData())
                    ->setVilleartisan($form->get('villeartisan')->getData())
                    ->setLoginartisan($form->get('loginartisan')->getData())
                    ->setMdpartisan($form->get('mdpartisan')->getData())
                    ->setIdcorpsmetier($form->get('idcorpsmetier')->getData());

            $em->persist($artisan);
            $em->flush();
                
          return new Response('Vos Coordonnées ont bien été modifié');
    }
    
    //Générer le html du formulaire crée
    $formView = $form->createView();
    
    //Rendre la vue
    return $this->render('@BiEt/Artisan/majCoordonnee.html.twig', array
            ('form'=>$formView));
    }    
    
}
