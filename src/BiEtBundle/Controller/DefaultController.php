<?php

namespace BiEtBundle\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction(){
        return $this->render('@BiEt/Default/index.html.twig');
    }
    
    public function accueilArtisanAction(){
        $content = $this->renderView('@BiEt/Artisan/accueilArtisan.html.twig');
        return new Response($content);
    }
    
    public function accueilEntrepreneurAction(){
        return $this->redirectToRoute('bi_et_entrepreneur');
    }
    
    public function accueilGestionnaireAction(){
        $response = $this->forward('BiEtBundle:Gestionnaire:accueil');
        return $response;
    }
}
