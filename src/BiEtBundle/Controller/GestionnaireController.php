<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace BiEtBundle\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use BiEtBundle\Entity\Secteur;
use BiEtBundle\Form\SecteurType;
use BiEtBundle\Entity\Corpsmetier;
use BiEtBundle\Form\CorpsmetierType;
/**
 * Description of GestionnaireController
 *
 * @author developpeur
 */

class GestionnaireController extends Controller{
    
    // Page d'accueil connexion
    public function accueilAction(Request $requete){
        $form = $this->createFormBuilder()
                ->add('login', TextType::class)
                ->add('mdp', PasswordType::class)
                ->add('Envoyer', SubmitType::class)
                ->getForm();
                
        $form->handleRequest($requete);
        

        if($form->isSubmitted()){
            $data = $form->getData();
            $login = $data['login'];
            $mdp = $data['mdp'];
            
            if($mdp == "azerty" && $login="admin"){
                return $this->render('@BiEt/Gestionnaire/sectionAccueil.html.twig');
            }
            
            else{
                return new response("Login ou mot de passe incorrect, veuillez recommencer");
            }
        }        
        
        return $this->render('@BiEt/Gestionnaire/indexGestionnaire.html.twig', array('form'=>$form->createView()));
    }
    
    
    
    //Initialiser les corps métiers
    public function initCMAction(){
        $em = $this->getDoctrine()
                    ->getManager();
        
        $corpsMetier1 = new Corpsmetier();
        $corpsMetier1->setLibellecorpsmetier("charpentier bois");
        $corpsMetier2 = new Corpsmetier();
        $corpsMetier2->setLibellecorpsmetier("construction bois");
        $corpsMetier3 = new Corpsmetier();
        $corpsMetier3->setLibellecorpsmetier("construction béton armé");
        $corpsMetier4 = new Corpsmetier();
        $corpsMetier4->setLibellecorpsmetier("couvreur");
        $corpsMetier5 = new Corpsmetier();
        $corpsMetier5->setLibellecorpsmetier("etancheur");
        $corpsMetier6 = new Corpsmetier();
        $corpsMetier6->setLibellecorpsmetier("miroiter");
        $corpsMetier7 = new Corpsmetier();
        $corpsMetier7->setLibellecorpsmetier("chauffage");
        $corpsMetier8 = new Corpsmetier();
        $corpsMetier8->setLibellecorpsmetier("electricien");
        
        $em->persist($corpsMetier1);
        $em->persist($corpsMetier2);
        $em->persist($corpsMetier3);
        $em->persist($corpsMetier4);
        $em->persist($corpsMetier5);
        $em->persist($corpsMetier6);
        $em->persist($corpsMetier7);
        $em->persist($corpsMetier8);
        $em->flush();
        
      return $this->render('@BiEt/Gestionnaire/indexGestionnaire.html.twig');
    }
    
    public function afficherSectionAccueilAction(){
        return $this->render('@BiEt/Gestionnaire/sectionAccueil.html.twig');
    }
    
    
    
    // Afficher les corps métiers et secteurs après le click
    public function afficherCmSecteurAction(){
        //Contient le dépôt (endroit ou on peut récuper les 
        //requête 'sql') concernant la requête
        $repository1 = $this->getDoctrine()
                            ->getRepository('BiEtBundle:Corpsmetier');         
        $lesCorpsMetiers = $repository1->findAll();

        $repository2 = $this->getDoctrine()
                            ->getRepository('BiEtBundle:Secteur');         
        $secteurs = $repository2->findAll();
        
        return $this->render('@BiEt/Gestionnaire/afficherCmSecteur.html.twig', array
                ('lesCorpsMetiers'=>$lesCorpsMetiers, 
                 'secteurs'=>$secteurs));
    }
    
    
    // Afficher les entrepreneurs et secteurs après le click
    public function afficherEntrepreneurAction(){
        $repository = $this->getDoctrine()
                            ->getRepository('BiEtBundle:Entrepreneur');         
        $lesEntrepreneurs = $repository->findAll();
        
        return $this->render('@BiEt/Gestionnaire/afficherEntrepreneur.html.twig', array
                ('lesEntrepreneurs'=>$lesEntrepreneurs));
    }
    
    
    // Afficher les artisans et secteurs après le click
    public function afficherArtisanAction(){
        $repository = $this->getDoctrine()
                            ->getRepository('BiEtBundle:Artisan');         
        $lesArtisans = $repository->findAll();
        
        return $this->render('@BiEt/Gestionnaire/afficherArtisan.html.twig', array
                ('lesArtisans'=>$lesArtisans));
    }

    
    //Création d'un formulaire et qui va 
    //appeler la méthode en dessous (ajouter)
    public function createFormCorpsMetierAction(Request $requete){
        $addCorpsMetier = new Corpsmetier(); 
        $form = $this->createForm(CorpsmetierType::class, $addCorpsMetier)
                     ->add('Envoyer', SubmitType::class);

        $form->handleRequest($requete);
        
        //if($form->isSubmitted()){
        //    $this->addCorpsMetierAction($addCorpsMetier);
        //}
        
        return $this->render('@BiEt/Gestionnaire/corpsMetierAjouter.html.twig', array('form'=>$form->createView()));
    }
    
    
    
    //Ajoute un corps métier dans la base de donnée
    public function addCorpsMetierAction(Corpsmetier $addCorpsMetier){
        //$form = $this->createForm(CorpsmetierType::class, $addCorpsMetier);
        //$form->handleRequest($request);
        
        //if($form->isSubmitted()){
            $em = $this->getDoctrine()
                        ->getManager();
            
            $em->persist($addCorpsMetier);
            $em->flush();
            return new Response('Corps métier ajouté');
        
        
        //$formView = $form->createView();
        //return $this->render('@BiEt/Gestionnaire/corpsMetierAjouter.html.twig', array
        //    ('form'=>$formView));
    }
    
    
    //Initialise les secteurs
    public function initSecteurAction(){
        $em = $this->getDoctrine()
                    ->getManager();
        
        $secteur1 = new Secteur();
        $secteur1->setLibellesecteur("Structure et gros oeuvre");
        $secteur2 = new Secteur();
        $secteur2->setLibelleSecteur("Enveloppe extérieur");
        $secteur3 = new Secteur();
        $secteur3->setLibelleSecteur("Equipement technique");
        
        $em->persist($secteur1);
        $em->persist($secteur2);
        $em->persist($secteur3);
        
        $em->flush();
        
    }
    
    
    
    //Ajouter un secteur dans la base de donnée
    public function addSecteurAction(Request $request){
        $addSecteur = new Secteur();
        $form = $this->createForm(SecteurType::class, $addSecteur)
                     ->add('Envoyer', SubmitType::class);

        $form->handleRequest($request);
        
        if($form->isSubmitted()){
            $em = $this->getDoctrine()
                        ->getManager();
            
            $em->persist($addSecteur);
            $em->flush();
            return new Response('Secteur ajouté');
        }
        
        $formView = $form->createView();
        return $this->render('@BiEt/Gestionnaire/secteurAjouter.html.twig', array
            ('form'=>$formView));
    }
    
    
    
    //Supprime un corps métier
    public function removeCorpsMetierAction(Request $request) {                  
    
    $form = $this->createForm()
                ->add('corpsMetier', EntityType::class,
                        array('label'=>'Corps Métiers',
                              'class'=>'BiEtBundle:CorpsMetier',
                              'choice_label'=>'libelleCm',
                              'multiple'=>false))
                ->add('Supprimer', SubmitType::class)
                ->getForm();
    
    $form->handleRequest($request);
    
    if($form->isSubmitted()){
        //Récupère l'id correspondant à l'item dans la liste déroulante
        $id = $form->get('corpsMetier')->getData()->getId();
        $em = $this->getDoctrine()->getEntityManager();
        
        //Récupère l'object d'entité correspondant
        $
        
        //Supprime l'object d'entité
        $em->remove($id);
        $em->flush();
        return new Response('Corps métier supprimé');
    }
    
    return $this->render('@BiEt/Gestionnaire/secteurAjouter.html.twig');

    }
}
